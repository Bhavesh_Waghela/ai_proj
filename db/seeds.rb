# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

question1 = Question.where(question: "").first_or_create
Answer.first_or_create(question_id: question1.id, answer: "Sorry I didn’t get that right now! i'll work on it.")

question2 = Question.where(question: "hi").first_or_create
Answer.first_or_create(question_id: question2.id, answer: "Hello, dear thanks for reaching me!")