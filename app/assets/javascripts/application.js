// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require rails-ujs
//= require turbolinks
//= require_tree .

document.addEventListener('DOMContentLoaded', function(){
  Typed.new("#typed", {
    stringsElement: document.getElementById('typed-strings'),
    typeSpeed: 30,
    backDelay: 700,
    loop: false,
    contentType: 'html', // or text
    // defaults to null for infinite loop
    loopCount: null,
    callback: function(){ foo(); },
    resetCallback: function() { newTyped(); }
  });

  var resetElement = document.querySelector('.reset');
  if(resetElement) {
    resetElement.addEventListener('click', function() {
      document.getElementById('typed')._typed.reset();
    });
  }
});

function newTyped(){}
function foo(){}

function myfunction(){
  $('#new_question').submit();
}
