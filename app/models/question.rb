class Question < ApplicationRecord
    has_one :answer

    def check_avalibilty_of_question
      greetings = ["HI", "hi", "Hi", "Hiee", "hiee", "hie", "HELLO", "hello", "howdy", "HOWDY", "kem cho"]
      if greetings.any? { |w| 
          @string = self.question[w] 
        }
        if @string
          @question = Question.where(question: @string.downcase).first
          answer = @question.answer
        else
          answer = Answer.first.answer
        end
      else
        answer = Answer.first.answer
      end  
      return answer
    end
end
