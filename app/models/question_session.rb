class QuestionSession
  def initialize(session)
    @session = session
    @session[:question_id] ||= []
  end
  
  def add_to_list(guest_question)
    @session[:question_id] << guest_question.id
  end
  
  def remove_from_list(guest_question)
    @session.delete(guest_question.id)
  end
  
  def current_questions
    @session[:question_id]
  end
end