class DashboardController < ApplicationController
  def index
    if question_session.current_questions
      @questions = Question.where('id IN (?)', question_session.current_questions).order('id DESC')
      
      @questions.each do |question|
        if question.created_at <= Time.zone.now - 5*60
          question_session.remove_from_list(question)
        end
      end
    end
    @question = Question.new
  end
end
