class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :question_session

  def question_session
    @question_session ||= QuestionSession.new(session)
  end
end
